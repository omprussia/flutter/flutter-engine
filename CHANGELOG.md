## Updated: 04/22/2024 15:44:57 PM

## Info

- Last tag: 3.16.2-2
- Released: 5

## Versions

- Version: 3.16.2-2 (28/12/2023)
- Version: 3.16.2-1 (26/12/2023)
- Version: 3.16.2 (26/12/2023)
- Version: 3.13.5 (26/12/2023)
- Version: 3.3.10 (26/12/2023)

### Version: 3.16.2-2 (28/12/2023)

#### Feature

- Add changeln config and templates.

### Version: 3.16.2-1 (26/12/2023)

#### Change

- Update engine to Flutter SDK 3.16.2-1

### Version: 3.16.2 (26/12/2023)

#### Change

- Update engine to Flutter SDK 3.16.2

### Version: 3.13.5 (26/12/2023)

#### Change

- Update engine to Flutter SDK 3.13.5

### Version: 3.3.10 (26/12/2023)

#### Change

- Add engine Flutter SDK 3.3.10
