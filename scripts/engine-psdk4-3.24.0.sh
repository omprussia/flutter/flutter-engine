#!/bin/bash

# SPDX-FileCopyrightText: Copyright 2023 Open Mobile Platform LLC <community@omp.ru>
# SPDX-License-Identifier: BSD-3-Clause

########################################################
## Links
## You must add links to the tools and targets available to you
## URL_TOOLING, URL_TARGET_aarch64, URL_TARGET_armv7hl, URL_TARGET_x86_64

########################################################
########################################################

## Engine tag version
## Flutter SDK -> bin/internal/engine.version
ENGINE_TAG="b8800d88be4866db1b15f8b954ab2573bba9960f"

## Install apps
ENABLE_APT=false

## Clear engine
ENABLE_CLEAN_ENGINE=false

## Clear tooling and targets
ENABLE_CLEAN_TOOLING=true

## Enable architecture-specific builds
BUILD_ARM=true

########################################################
########################################################

## Platform SDK Tooling and Targets
URL_TOOLING="https://sdk-repo.omprussia.ru/sdk/installers/4.1.0/PlatformSDK/Aurora_OS-4.1.0.27-base-Aurora_Platform_SDK_Chroot-i486.tar.bz2"
URL_TARGET_armv7hl="https://sdk-repo.omprussia.ru/sdk/installers/4.1.0/PlatformSDK/Aurora_OS-4.1.0.27-base-Aurora_SDK_Target-armv7hl.tar.7z"

EXT_TAR_7Z='.tar.7z'
EXT_TAR_BZ2='.tar.bz2'

##### Install apps

if [ "$ENABLE_APT" = true ] ; then
    sudo apt install wget git curl unzip pkg-config p7zip-full
fi

##### Clear engine

if [ "$ENABLE_CLEAN_ENGINE" = true ] ; then
    rm -rf src
    rm -rf .cipd
    rm -rf .gclien*
    rm -rf depot_tools
    rm -rf _bad_scm
fi

########################################################
##### Get Platform SDK

declare -A install_tooling=(
    ["url"]="$URL_TOOLING"
    ["ext"]="$EXT_TAR_BZ2"
)

declare -A install_target_arm=(
    ["url"]="$URL_TARGET_armv7hl"
    ["ext"]="$EXT_TAR_7Z"
)

urls=('tooling')
if [ "$BUILD_ARM" = true ] ; then
    urls+=('target_arm')
fi

for item in ${urls[@]}; do

    build_name="install_$item"
    declare -n build=$build_name

    url=${build['url']}
    ext=${build['ext']}

    archive=$(basename $url)
    folder=$(basename $url $ext)

    if [ "$ENABLE_CLEAN_TOOLING" = true ] ; then
        sudo rm -rf $folder
    fi

    ## Downloads tooling
    if [ ! -f "$PWD/$archive" ]; then
        curl -O $url
    fi

    ## Extract tooling
    if [ ! -d "$PWD/$folder" ]; then
        extract_name="${archive%.*}"

        rm -rf "$extract_name"
        rm -rf "$extract_name.meta"

        if [ "$ext" = "$EXT_TAR_7Z" ] ; then
          7za x $archive
        fi
        if [ "$ext" = "$EXT_TAR_BZ2" ] ; then
          bzip2 -dk $archive
        fi

        mkdir $folder
        { tar -xf $extract_name -C $folder | grep "something" ;} 2>/dev/null

        rm -rf "$extract_name"
        rm -rf "$extract_name.meta"
    fi
done

########################################################
##### Get Engine

## Get depot_tools
if [ ! -d "$PWD/depot_tools" ]; then
    git clone https://chromium.googlesource.com/chromium/tools/depot_tools.git
fi

## Export depot_tools
export PATH=$PWD/depot_tools:$PATH

## Add config
if [ ! -f "$PWD/.gclient" ]; then
cat <<EOT >> .gclient
solutions = [
{
    "managed": False,
    "name": "src/flutter",
    "url": "git@github.com:flutter/engine.git@$ENGINE_TAG",
    "custom_deps": {},
    "deps_file": "DEPS",
    "safesync_url": "",
    "custom_vars": {
    "download_android_deps": False,
    "download_windows_deps": False,
    }
},
]
EOT
fi

########################################################
##### Gclient sync

## Remove pathes if exist
python3 $PWD/scripts/patcher.py --revert

## Run gclient update
gclient sync

## Check resources
if [ ! -d "$PWD/src" ]; then
    echo 'Error gclient sync'
    exit 1
fi

# Apply pathes
python3 $PWD/scripts/patcher.py --apply

########################################################
##### Run build

MODES=('release' 'profile' 'debug')

builds=()

if [ "$BUILD_ARM" = true ] ; then
    builds+=('arm')
fi

#######
### ARM

declare -A build_arm=(
    ["path"]="src/out/aurora_type_arm"
    ["triple"]="armv7-unknown-linux-gnueabihf"
    ["tools-triple"]="armv7hl-meego-linux-gnueabi"
    ["folder"]="$(basename $URL_TARGET_armv7hl $EXT_TAR_7Z)"
)

CLANG_TOOLING=$PWD/src/flutter/buildtools/linux-x64/clang/

## Run build arm/arm64
for cpu in ${builds[@]}
do
    build_name="build_$cpu"
    declare -n build=$build_name

    triple=${build['triple']}
    tools_triple=${build['tools-triple']}
    folder=${build['folder']}

    ## Symlinks tooling

    echo "Create symlinks..."
    TOOLING_FOLDER=$(basename $URL_TOOLING $EXT_TAR_BZ2)
    if [ ! -f "$CLANG_TOOLING/bin/$triple-ar" ]; then
        ln -s $PWD/$TOOLING_FOLDER/opt/cross/bin/$tools_triple-ar $CLANG_TOOLING/bin/$triple-ar
    fi
    if [ ! -f "$CLANG_TOOLING/bin/$triple-readelf" ]; then
        ln -s $PWD/$TOOLING_FOLDER/opt/cross/bin/$tools_triple-readelf $CLANG_TOOLING/bin/$triple-readelf
    fi
    if [ ! -f "$CLANG_TOOLING/bin/$triple-nm" ]; then
        ln -s $PWD/$TOOLING_FOLDER/opt/cross/bin/$tools_triple-nm $CLANG_TOOLING/bin/$triple-nm
    fi
    if [ ! -f "$CLANG_TOOLING/bin/$triple-strip" ]; then
        ln -s $PWD/$TOOLING_FOLDER/opt/cross/bin/$tools_triple-strip $CLANG_TOOLING/bin/$triple-strip
    fi

    ### Run GN
    for mode in ${MODES[@]}
    do

        arg=''
        path=$(echo ${build['path']} | sed s/type/$mode/g)

        if [ "$mode" == "debug" ]; then
            arg='--unoptimized';
            path=$(echo ${build['path']} | sed s/type/${mode}_unopt/g)
        fi

        if [ "$mode" == "profile" ]; then
            arg='--no-lto';
        fi

        echo
        echo 'RUN...'
        echo "arg: $arg"
        echo "cpu: $cpu"
        echo "path: $path"
        echo "triple: $triple"
        echo "folder: $folder"
        echo

        rm -rf $path

        ./src/flutter/tools/gn  \
            --runtime-mode $mode $arg \
            --target-os aurora \
            --linux-cpu $cpu \
            --arm-float-abi hard \
            --no-enable-unittests \
            --embedder-for-target \
            --disable-desktop-embeddings \
            --no-build-embedder-examples \
            --enable-fontconfig \
            --no-goma \
            --no-prebuilt-dart-sdk \
            --target-toolchain $CLANG_TOOLING \
            --target-sysroot $PWD/$folder \
            --target-triple $triple

        ninja -C $path

    done
done
