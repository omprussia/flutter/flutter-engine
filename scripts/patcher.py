#!/usr/bin/env python3

# SPDX-FileCopyrightText: Copyright 2023 Open Mobile Platform LLC <community@omp.ru>
# SPDX-License-Identifier: BSD-3-Clause

import sys
import os
import subprocess
import shutil
from argparse import ArgumentParser
from pathlib import Path

patches = {
  'src' : ['0001-buildroot-Add-build-configuration-for-Aurora-OS.patch'],
  'src/flutter' : [ '0002-engine-Fixed-build-scripts-for-Aurora-OS.patch',
                    '0003-engine-Add-physical-view-paddings-to-the-FlutterWind.patch' ],
  'src/flutter/third_party/dart': [ '0004-dart-Fixed-build-for-Aurora-OS.patch' ]
}

def Run(
  args: list,
  cwd: Path = Path(os.getcwd()),
  ignore_error:bool = False,
  verbose:bool = False,
  error_message: str = '',
  env=os.environ
) :
  result = []
  with subprocess.Popen(args, cwd=cwd, env=env, stdout=subprocess.PIPE, stderr=subprocess.STDOUT) as p:
    for raw_line in iter(lambda: p.stdout.readline(), ""):
        if not raw_line:
            break
        line = raw_line.decode('utf-8').rstrip()
        if verbose:
          print(line)
        result.append(line.strip())

  if p.returncode != 0:
    if ignore_error:
      return []
    raise OSError(p.returncode, error_message if error_message else 'Cannot run command: ' + ' '.join(args))

  return result

def ApplyPatches(cwd: Path = Path(os.getcwd()), verbose: bool = False):
  if verbose:
      print(f'Apply patches...')
  for repo in patches:
    repo_path = cwd / repo
    if repo_path.exists():
      for patch in patches[repo]:
        patch_path = cwd / 'patches' / patch
        args = ['git', 'apply', patch_path]
        Run(args=args, cwd=repo_path, verbose=verbose, error_message=f'Cannot apply patch {patch_path} for {repo_path}')


def RemovePatches(cwd: Path = Path(os.getcwd()), verbose: bool = False):
  if verbose:
      print(f'Revert patches...')
  for repo in patches:
    repo_path = cwd / repo
    if repo_path.exists():
      args = ['git', 'reset', 'HEAD', '--hard']
      Run(args=args, cwd=repo_path, verbose=verbose, error_message=f'Cannot reset source changes for {repo_path}')

def main():
  parser = ArgumentParser()
  parser.add_argument('--apply', action='store_true', dest='apply',  default=False)
  parser.add_argument('--revert', action='store_true', dest='revert', default=False)
  parser.add_argument('-v', '--verbose', action='store_true', dest='verbose', default=False)

  options = parser.parse_args()

  try:
    working_dir = Path(os.getcwd())
    print(f'Current working dir: {working_dir}')

    if (options.apply):
      ApplyPatches(cwd=working_dir, verbose=options.verbose)

    if (options.revert):
      RemovePatches(cwd=working_dir, verbose=options.verbose)

    return 0

  except OSError as err:
    print(err.strerror)
    return 1
  except RuntimeError as err:
    print(err.args[0])
    return 1


if __name__ == '__main__':
  sys.exit(main())
