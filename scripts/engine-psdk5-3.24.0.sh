#!/bin/bash

# SPDX-FileCopyrightText: Copyright 2023 Open Mobile Platform LLC <community@omp.ru>
# SPDX-License-Identifier: BSD-3-Clause

########################################################
## Links
## You must add links to the tools and targets available to you
## URL_TOOLING, URL_TARGET_aarch64, URL_TARGET_armv7hl, URL_TARGET_x86_64

########################################################
########################################################

## Engine tag version
## Flutter SDK -> bin/internal/engine.version
ENGINE_TAG="b8800d88be4866db1b15f8b954ab2573bba9960f"

## Install apps
ENABLE_APT=false

## Clear engine
ENABLE_CLEAN_ENGINE=false

## Clear tooling and targets
ENABLE_CLEAN_TOOLING=false

## Enable architecture-specific builds
BUILD_ARM=true
BUILD_ARM64=true
BUILD_X64=true

########################################################
########################################################

## Platform SDK Tooling and Targets
URL_TOOLING="https://sdk-repo.omprussia.ru/sdk/installers/5.1.0/PlatformSDK/5.1.0.24/Aurora_OS-5.1.0.24-base-Aurora_SDK_Tooling-x86_64.tar.7z"
URL_TARGET_aarch64="https://sdk-repo.omprussia.ru/sdk/installers/5.1.0/PlatformSDK/5.1.0.24/Aurora_OS-5.1.0.24-base-Aurora_SDK_Target-aarch64.tar.7z"
URL_TARGET_armv7hl="https://sdk-repo.omprussia.ru/sdk/installers/5.1.0/PlatformSDK/5.1.0.24/Aurora_OS-5.1.0.24-base-Aurora_SDK_Target-armv7hl.tar.7z"
URL_TARGET_x86_64="https://sdk-repo.omprussia.ru/sdk/installers/5.1.0/PlatformSDK/5.1.0.24/Aurora_OS-5.1.0.24-base-Aurora_SDK_Target-x86_64.tar.7z"

TAR='.tar.7z'

## Folders

TOOLING_FOLDER=$(basename $URL_TOOLING $TAR)
TARGET_aarch64_FOLDER=$(basename $URL_TARGET_aarch64 $TAR)
TARGET_armv7hl_FOLDER=$(basename $URL_TARGET_armv7hl $TAR)
TARGET_x86_64_FOLDER=$(basename $URL_TARGET_x86_64 $TAR)

##### Install apps

if [ "$ENABLE_APT" = true ] ; then
    sudo apt install wget git curl unzip pkg-config p7zip-full
fi

##### Clear tooling and targets

if [ "$ENABLE_CLEAN_TOOLING" = true ] ; then
    rm -rf $TOOLING_FOLDER
    rm -rf $TARGET_aarch64_FOLDER
    rm -rf $TARGET_armv7hl_FOLDER
    rm -rf $TARGET_x86_64_FOLDER
fi

##### Clear engine

if [ "$ENABLE_CLEAN_ENGINE" = true ] ; then
    rm -rf src
    rm -rf .cipd
    rm -rf .gclien*
    rm -rf depot_tools
    rm -rf _bad_scm
fi

########################################################
##### Get Platform SDK

urls=($URL_TOOLING $URL_TARGET_aarch64 $URL_TARGET_armv7hl $URL_TARGET_x86_64)

for url in ${urls[@]}; do

    tar=$(basename $url)
    folder=$(basename $url $TAR)

    ## Downloads tooling
    if [ ! -f "$PWD/$tar" ]; then
        curl -O $url
    fi

    ## Extract tooling
    if [ ! -d "$PWD/$folder" ]; then
        extract_name="${tar%.*}"

        rm -rf "$extract_name"
        rm -rf "$extract_name.meta"

        7za x $tar
        mkdir $folder
        { tar -xf $extract_name -C $folder | grep "something" ;} 2>/dev/null

        rm -rf "$extract_name"
        rm -rf "$extract_name.meta"
    fi
done

########################################################
##### Get Engine

## Get depot_tools
if [ ! -d "$PWD/depot_tools" ]; then
    git clone https://chromium.googlesource.com/chromium/tools/depot_tools.git
fi

## Export depot_tools
export PATH=$PWD/depot_tools:$PATH

## Add config
if [ ! -f "$PWD/.gclient" ]; then
cat <<EOT >> .gclient
solutions = [
{
    "managed": False,
    "name": "src/flutter",
    "url": "git@github.com:flutter/engine.git@$ENGINE_TAG",
    "custom_deps": {},
    "deps_file": "DEPS",
    "safesync_url": "",
    "custom_vars": {
    "download_android_deps": False,
    "download_windows_deps": False,
    }
},
]
EOT
fi

########################################################
##### Gclient sync

## Remove pathes if exist
python3 $PWD/scripts/patcher.py --revert

## Run gclient update
gclient sync

## Check resources
if [ ! -d "$PWD/src" ]; then
    echo 'Error gclient sync'
    exit 1
fi

# Apply pathes
python3 $PWD/scripts/patcher.py --apply

########################################################
##### Run build

MODES=('release' 'profile' 'debug')

builds=()

if [ "$BUILD_ARM" = true ] ; then
    builds+=('arm')
fi

if [ "$BUILD_ARM64" = true ] ; then
    builds+=('arm64')
fi

if [ "$BUILD_X64" = true ] ; then
    builds+=('x64')
fi

#######
### ARM

declare -A build_arm=(
    ["path"]="src/out/aurora_type_arm"
    ["triple"]="armv7-unknown-linux-gnueabihf"
    ["tools-triple"]="armv7hl-meego-linux-gnueabi"
    ["folder"]="$TARGET_armv7hl_FOLDER"
)

declare -A build_arm64=(
    ["path"]="src/out/aurora_type_arm64"
    ["triple"]="aarch64-unknown-linux-gnu"
    ["tools-triple"]="aarch64-meego-linux-gnu"
    ["folder"]="$TARGET_aarch64_FOLDER"
)

declare -A build_x64=(
    ["path"]="src/out/aurora_type_x64"
    ["triple"]="x86_64-unknown-linux-gnu"
    ["tools-triple"]="x86_64-meego-linux-gnu"
    ["folder"]="$TARGET_x86_64_FOLDER"
)

CLANG_TOOLING=$PWD/src/flutter/buildtools/linux-x64/clang/

## Run build arm/arm64
for cpu in ${builds[@]}
do
    build_name="build_$cpu"
    declare -n build=$build_name

    triple=${build['triple']}
    tools_triple=${build['tools-triple']}
    folder=${build['folder']}

    ## Symlinks tooling

    echo "Create symlinks..."
    if [ ! -f "$CLANG_TOOLING/bin/$triple-ar" ]; then
        ln -s $PWD/$TOOLING_FOLDER/opt/cross/bin/$tools_triple-ar $CLANG_TOOLING/bin/$triple-ar
    fi
    if [ ! -f "$CLANG_TOOLING/bin/$triple-readelf" ]; then
        ln -s $PWD/$TOOLING_FOLDER/opt/cross/bin/$tools_triple-readelf $CLANG_TOOLING/bin/$triple-readelf
    fi
    if [ ! -f "$CLANG_TOOLING/bin/$triple-nm" ]; then
        ln -s $PWD/$TOOLING_FOLDER/opt/cross/bin/$tools_triple-nm $CLANG_TOOLING/bin/$triple-nm
    fi
    if [ ! -f "$CLANG_TOOLING/bin/$triple-strip" ]; then
        ln -s $PWD/$TOOLING_FOLDER/opt/cross/bin/$tools_triple-strip $CLANG_TOOLING/bin/$triple-strip
    fi

    ### Run GN
    for mode in ${MODES[@]}
    do

        arg=''
        path=$(echo ${build['path']} | sed s/type/$mode/g)

        if [ "$mode" == "debug" ]; then
            arg='--unoptimized';
            path=$(echo ${build['path']} | sed s/type/${mode}_unopt/g)
        fi

        if [ "$mode" == "profile" ]; then
            arg='--no-lto';
        fi

        echo
        echo 'RUN...'
        echo "arg: $arg"
        echo "cpu: $cpu"
        echo "path: $path"
        echo "triple: $triple"
        echo "folder: $folder"
        echo

        rm -rf $path

        ./src/flutter/tools/gn  \
            --runtime-mode $mode $arg \
            --target-os aurora \
            --linux-cpu $cpu \
            --arm-float-abi hard \
            --no-enable-unittests \
            --embedder-for-target \
            --disable-desktop-embeddings \
            --no-build-embedder-examples \
            --enable-fontconfig \
            --no-goma \
            --no-prebuilt-dart-sdk \
            --target-toolchain $CLANG_TOOLING \
            --target-sysroot $PWD/$folder \
            --target-triple $triple

        ninja -C $path

    done
done
