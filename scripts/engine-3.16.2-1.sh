#!/bin/bash

# SPDX-FileCopyrightText: Copyright 2023 Open Mobile Platform LLC <community@omp.ru>
# SPDX-License-Identifier: BSD-3-Clause

########################################################
## You mast have GLIBC >= 2.38 (Ubuntu 23.10)

########################################################
## You must install clang-18
##
## wget https://apt.llvm.org/llvm.sh
## chmod +x llvm.sh
## sudo ./llvm.sh 18
## sudo update-alternatives --install /usr/bin/clang clang /usr/bin/clang-18 100
## sudo update-alternatives --install /usr/bin/clang++ clang++ /usr/bin/clang++-18 100

########################################################
## Patches
## To apply patches, create a folder in the script directory and add patches for the engine and its assembly there.
## https://gitlab.com/omprussia/flutter/flutter-engine/-/tree/dev/patches

########################################################
## Links
## You must add links to the tools and targets available to you
## URL_TOOLING, URL_TARGET_aarch64, URL_TARGET_armv7hl

########################################################
########################################################

## Engine tag version
## Flutter SDK -> bin/internal/engine.version
ENGINE_TAG="cf7a9d0800f2a5da166dbe0eb9fb2476018269b1"

## Install apps
ENABLE_APT=false

## Clear engine
ENABLE_CLEAN_ENGINE=false

## Clear tooling and targets
ENABLE_CLEAN_TOOLING=false

## Build ARM
BUILD_ARM=true

## Build ARM
BUILD_ARM64=true

## Build ARM
BUILD_X64=true

########################################################
########################################################

## Platform SDK Tooling and Targets
URL_TOOLING=""
URL_TARGET_aarch64=""
URL_TARGET_armv7hl=""

TAR='.tar.7z'

## Folders

TOOLING_FOLDER=$(basename $URL_TOOLING $TAR)

TARGET_aarch64_FOLDER=$(basename $URL_TARGET_aarch64 $TAR)
TARGET_armv7hl_FOLDER=$(basename $URL_TARGET_armv7hl $TAR)

patchAdd () {
    if [ -d "$PWD/src" ] && [ -d "$PWD/patches" ]; then
        for i in patches/*.patch; do patch -p1 < $i; done
    fi
}

patchDel () {
    if [ -d "$PWD/src" ] && [ -d "$PWD/patches" ]; then
        for i in patches/*.patch; do patch -s -r /tmp/deleteme.rej --forward --reverse -p1 < $i; done
    fi
}

##### Install apps

if [ "$ENABLE_APT" = true ] ; then
    sudo apt install wget git curl unzip pkg-config p7zip-full
fi

##### Clear tooling and targets

if [ "$ENABLE_CLEAN_TOOLING" = true ] ; then
    rm -rf $TOOLING_FOLDER
    rm -rf $TARGET_aarch64_FOLDER
    rm -rf $TARGET_armv7hl_FOLDER
fi

##### Clear engine

if [ "$ENABLE_CLEAN_ENGINE" = true ] ; then
    rm -rf src
    rm -rf .cipd
    rm -rf .gclien*
    rm -rf depot_tools
    rm -rf _bad_scm
fi

########################################################
##### Get Platform SDK

urls=($URL_TOOLING $URL_TARGET_aarch64 $URL_TARGET_armv7hl)

for url in ${urls[@]}; do

    tar=$(basename $url)
    folder=$(basename $url $TAR)

    ## Downloads tooling
    if [ ! -f "$PWD/$tar" ]; then
        curl $URL_TOOLING
    fi

    ## Extract tooling
    if [ ! -d "$PWD/$folder" ]; then
        extract_name="${tar%.*}"

        rm -rf "$extract_name"
        rm -rf "$extract_name.meta"

        7za x $tar
        mkdir $folder
        { tar -xf $extract_name -C $folder | grep "something" ;} 2>/dev/null

        rm -rf "$extract_name"
        rm -rf "$extract_name.meta"
    fi
done

########################################################
##### Get Engine

## Get depot_tools
if [ ! -d "$PWD/depot_tools" ]; then
    git clone https://chromium.googlesource.com/chromium/tools/depot_tools.git
fi

## Export depot_tools
export PATH=$PWD/depot_tools:$PATH

## Add config
if [ ! -f "$PWD/.gclient" ]; then
cat <<EOT >> .gclient
solutions = [
{
    "managed": False,
    "name": "src/flutter",
    "url": "git@github.com:flutter/engine.git@$ENGINE_TAG",
    "custom_deps": {},
    "deps_file": "DEPS",
    "safesync_url": "",
    "custom_vars": {
    "download_android_deps": False,
    "download_windows_deps": False,
    } 
},
]
EOT
fi

########################################################
##### Gclient sync

## Remove pathes if exist
patchDel

## Run gclient update
gclient sync -j 1

## Check resources
if [ ! -d "$PWD/src" ]; then
    echo 'Error gclient sync'
    exit 1
fi

# Apply pathes
patchAdd

########################################################
##### Run build

MODES=('release' 'profile' 'debug')

builds=()

if [ "$BUILD_ARM" = true ] ; then
    builds+=('arm')
fi

if [ "$BUILD_ARM64" = true ] ; then
    builds+=('arm64')
fi

#######
### ARM

declare -A build_arm=(
    ["path"]="src/out/linux_type_arm" 
    ["triple"]="armv7hl-meego-linux-gnueabi"
    ["folder"]="$TARGET_armv7hl_FOLDER"
)

declare -A build_arm64=(
    ["path"]="src/out/linux_type_arm64" 
    ["triple"]="aarch64-meego-linux-gnu"
    ["folder"]="$TARGET_aarch64_FOLDER"
)

## Run build arm/arm64
for cpu in ${builds[@]}
do
    build_name="build_$cpu"
    declare -n build=$build_name

    triple=${build['triple']}
    folder=${build['folder']}
    
    ## Symlinks tooling

    rm -rf $PWD/$TOOLING_FOLDER/opt/cross/$triple/bin
    rm -rf $PWD/$TOOLING_FOLDER/opt/cross/$triple/lib/gcc

    mkdir -p $PWD/$TOOLING_FOLDER/opt/cross/$triple/bin
    mkdir -p $PWD/$TOOLING_FOLDER/opt/cross/$triple/lib/gcc

    if [ "$cpu" == "arm64" ]; then
        ln -s $PWD/src/buildtools/linux-x64/clang/bin/clang++ $PWD/$TOOLING_FOLDER/opt/cross/$triple/bin/clang++
        ln -s $PWD/src/buildtools/linux-x64/clang/bin/clang $PWD/$TOOLING_FOLDER/opt/cross/$triple/bin/clang
    else
        ln -s /bin/clang++ $PWD/$TOOLING_FOLDER/opt/cross/$triple/bin/clang++
        ln -s /bin/clang $PWD/$TOOLING_FOLDER/opt/cross/$triple/bin/clang
    fi

    ln -s $PWD/$TOOLING_FOLDER/opt/cross/bin/* $PWD/$TOOLING_FOLDER/opt/cross/$triple/bin 2>/dev/null
    ln -s $PWD/$TOOLING_FOLDER/opt/cross/lib/gcc/$triple $PWD/$TOOLING_FOLDER/opt/cross/$triple/lib/gcc/$triple 2>/dev/null

    ### Run GN
    for mode in ${MODES[@]}
    do

        arg=''
        path=$(echo ${build['path']} | sed s/type/$mode/g)

        if [ "$mode" == "debug" ]; then
            arg='--unoptimized';
            path=$(echo ${build['path']} | sed s/type/${mode}_unopt/g)
        fi

        if [ "$mode" == "profile" ]; then
            arg='--no-lto';
        fi

        echo
        echo 'RUN...'
        echo "arg: $arg"
        echo "cpu: $cpu"
        echo "path: $path"
        echo "triple: $triple"
        echo "folder: $folder"
        echo

        rm -rf $path

        ./src/flutter/tools/gn  \
            --runtime-mode $mode $arg \
            --target-os linux \
            --linux-cpu $cpu \
            --arm-float-abi hard \
            --no-enable-unittests \
            --embedder-for-target \
            --disable-desktop-embeddings \
            --no-build-embedder-examples \
            --enable-fontconfig \
            --no-goma \
            --no-prebuilt-dart-sdk \
            --target-toolchain $PWD/$TOOLING_FOLDER/opt/cross/$triple \
            --target-sysroot $PWD/$folder \
            --target-triple $triple

        ninja -C $path

    done
done

##########
### X86_64

if [ "$BUILD_X64" = true ] ; then
    ## Run build X86_64
    for mode in ${MODES[@]}
    do

        arg=''
        path=$(echo "src/out/host_type" | sed s/type/$mode/g)

        if [ "$mode" == "debug" ]; then
            arg='--unoptimized';
            path="${path}_unopt"
        fi

        if [ "$mode" == "profile" ]; then
            arg='--no-lto';
        fi

        echo
        echo 'RUN...'
        echo "arg: $arg"
        echo "path: $path"
        echo

        rm -rf $path

        ./src/flutter/tools/gn  \
            --runtime-mode $mode $arg \
            --no-enable-unittests \
            --embedder-for-target \
            --disable-desktop-embeddings \
            --no-build-embedder-examples \
            --enable-fontconfig \
            --no-goma \
            --no-prebuilt-dart-sdk

        ninja -C $path

    done
fi

## Remove pathes
patchDel