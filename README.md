# Flutter Engine

![Version](https://img.shields.io/badge/dynamic/json?color=blue&label=Version&query=%24%5B%3A1%5D.name&url=https%3A%2F%2Fgitlab.com%2Fapi%2Fv4%2Fprojects%2F53055476%2Frepository%2Ftags)

[Flutter Engine](https://github.com/flutter/engine) — это портативная среда выполнения для приложений Flutter. Она реализует основные библиотеки Flutter, включая анимацию и графику, файловый и сетевой ввод-вывод, поддержку специальных возможностей, архитектуру плагинов, а также среду выполнения Dart.

Для самостоятельной сборки вы можете ознакомиться с документацией Flutter Engine - "[Compiling the engine](https://github.com/flutter/flutter/wiki/Compiling-the-engine)".

Для сборки можно воспользоваться кросс-компиляций доступной в [Аврора Platform SDK](https://developer.auroraos.ru/doc/software_development/psdk).
